

export function fetchAllProducts() {
  return new Promise(async(resolve) =>{
    const response= await fetch('http://localhost:8080/products');
    const data = await response.json();
    console.log("inside api",data)
    resolve({data})
  }
  );
}

export function fetchProductsByFilters(filter,sort,pagination) {
  //filter={category:["smartphone","laptop"]}
  //pagination:{_page:page,_limit:10}
  return new Promise(async(resolve) =>{
    let queryString="";
    for(let key in filter){
      //all catergory values which we have selected
      const categoryValue=filter[key]
      console.log("inside filterproductbyfilter:catergoryvalue",categoryValue)
      if(categoryValue.length){
        const lastcategoryValue=categoryValue[categoryValue.length-1] //only last categeory value select
        queryString +=`${key}=${lastcategoryValue}&`
      }
    }
    for(let key in sort){
      queryString +=`${key}=${sort[key]}&`
    }
    for(let key in pagination){
      queryString +=`${key}=${pagination[key]}&`
    }
    const response= await fetch('http://localhost:8080/products?'+queryString);
    const data = await response.json();
    // console.log("inside api",data)
    const totalItems= await response.headers.get('X-Total-Count');
    resolve({data:{products:data,totalItems:+totalItems}})
  }
  );
}