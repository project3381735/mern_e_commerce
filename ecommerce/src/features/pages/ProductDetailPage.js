import Navbar from "../navbar/Navbar";
import ProductDetail from "../product-list/components/ProductDetail";


function ProductDetailPage() {

    return ( 
        <Navbar>
            <ProductDetail></ProductDetail>
        </Navbar>

     );
}

export default ProductDetailPage;