import Navbar from "../navbar/Navbar";
import ProductList from "../product-list/components/ProductList";

function Home() {

    return ( 
        <Navbar>
            <ProductList></ProductList>
        </Navbar>

     );
}

export default Home;